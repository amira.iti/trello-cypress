
export function access_trello_board()
{
  cy.visit(Cypress.env('url'));
  cy.get(Cypress.env('selector_login')).should('be.visible');
  
}

export function view_card_details()
{
  cy.get(Cypress.env('selector_card')).click();
  cy.get(Cypress.env('selector_checklist')).should('be.visible');

  
}


<<<<<<< HEAD
=======
export function close_card_details()
{
  cy.get(Cypress.env('selector_close')).click();
  cy.get(Cypress.env('selector_card')).should('be.visible');

  
}
>>>>>>> 1-view-card-details

export function access_filter()
{
  cy.xpath(Cypress.env('selector_filter')).click();
  cy.xpath(Cypress.env('selector_keyword')).should('be.visible');

  
}

export function filter_by_keyword(keyword)
{
  cy.xpath(Cypress.env('selector_keyword')).type(Cypress.env(keyword));
  
}


export function close_filter()
{
  cy.get(Cypress.env('selector_close_filter')).should('be.visible');

  cy.get(Cypress.env('selector_close_filter')).click();
  
}
export function assert_filter_result()
{
  cy.xpath(Cypress.env('selector_result_assertion')).should('be.visible');
  
}


export function clear_filter()
{
  cy.xpath(Cypress.env('selector_clear_filter')).click();

  
}


export function filter_by_member(member_name)
{

  cy.xpath(Cypress.env('selector_keyword')).should('be.visible');

  cy.xpath(Cypress.env('selector_select_member')).type(Cypress.env(member_name));
  
}

export function assert_image_result()
{
  cy.xpath(Cypress.env('selected_member_img')).should('be.visible');
  
}

