const trello_lib = require('../../lib/trello/trello_lib');

describe('Filter By Member', () =>
{
  it('Access trello board ', () =>
  {
    cy.clearCookies()
    trello_lib.access_trello_board();
  })

  it('Access Filter ', () =>
  {
    trello_lib.access_filter();
  })

  it('Filter by Member ', () =>
  {
    trello_lib.filter_by_member('content_member_name');

  })


  it('Assert selected member image ', () =>
  {
    trello_lib.assert_image_result();

  })
  
  // it('Close Filter Pop-up ', () =>
  // {
  //   trello_lib.close_filter();
  // })
  


  it('Clear Filter Date ', () =>
  {
    trello_lib.clear_filter();
  })


})
