const trello_lib = require('../../lib/trello/trello_lib');

describe('View Card Details', () =>
{
  it('Access trello board ', () =>
  {
    trello_lib.access_trello_board();
  })

  it('View card with checklist ', () =>
  {
    trello_lib.view_card_details();
  })


  it('Close card ', () =>
  {
    trello_lib.close_card_details();
  })


})