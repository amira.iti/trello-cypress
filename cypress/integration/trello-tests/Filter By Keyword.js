const trello_lib = require('../../lib/trello/trello_lib');

describe('Filter', () =>
{
  it('Access trello board ', () =>
  {
    cy.clearCookies()
    trello_lib.access_trello_board();
  })

  it('Access Filter ', () =>
  {
    trello_lib.access_filter();
  })

  it('Filter by Keyword ', () =>
  {
    trello_lib.filter_by_keyword('content_keyword');

  })


  it('Assert filtered data ', () =>
  {
    trello_lib.assert_filter_result();

  })
  

  it('Clear Filter Date ', () =>
  {
    trello_lib.clear_filter();
  })

})
