# trello-test

## Functional Test Cases 
### View Card Detail


| Test Case Title.           | Test Case Precondition                               | Test Case Steps       | Expected Result            | 
| ------                     | ------                                               | ------               |     ------            | 
| Check to view card details | The board it created and has some cards added to it  | 1-Access the  board.      2-Access any card to view its details        |    All card details should be shown including the card details,assignee,due date,desc,...           | 
||  | 2-Access any card to view its details       |  


### Filter By a Keyword



| Test Case Title.  | Test Case Precondition | Test Case Steps   | Expected Result       | 
| ------            | ------              | ------               |     ------            | 
| Check to find a card by a keyword | The board is creaded and has some cards| 1-Access the board.  |   All cards having the entered keyword should be shown                    | 
|  | | 2-Click Filter  |                       | 
|  | | 3-Enter Keyword for exampke "Test9"  |                       | 





### Filter By a board member



| Test Case Title.  | Test Case Precondition | Test Case Steps   | Expected Result       | 
| ------            | ------              | ------               |     ------            | 
| Check to find a card by a member | The board is creaded and has some cards| 1-Access the board.  |   All cards having the selected member should be shown                    | 
|  | | 2-Click Filter  |                       | 
|  | | 3-Select the board member  |                       | 




## Setup the project


- [ ] Instal NodeJS based on your operation system

- For More details  https://www.newline.co/@Adele/how-to-install-nodejs-and-npm-on-macos--22782681

- [ ] git clone https://gitlab.com/amira.iti/trello-cypress.git
- [ ]  cd /trello-cypress
- [ ] npm install cypress
- [ ] Install Xpath package 

  `npm install -D cypress-xpath`

- Then include in your project's cypress/support/index.js

  `require('cypress-xpath')`

- An Example to use the Xpath

```
cy.xpath('//body')
cy.xpath('//script')
```

- For more details https://www.npmjs.com/package/cypress-xpath
- [ ] npx cypress open
- [ ] git checkout develop 

`Hint:The develop branch has all updates`


